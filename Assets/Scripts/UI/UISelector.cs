﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UISelector : MonoBehaviour, IPointerClickHandler
{
    public TileSelectedEvent OnTileSelected;

    public Vector2Int Tile { get { return _tile; } }
    private Vector2Int _tile = new Vector2Int(-1, -1);

    [SerializeField]
    private Color _selectedColor;
    [SerializeField]
    private Color _startColor;

    private MeshRenderer _renderer;
    private bool _interactive = true;

    private void Start()
    {
        _renderer = GetComponent<MeshRenderer>();
        _renderer.material.color = _startColor;
        if (!_interactive)
        {
            GetComponent<BoxCollider>().enabled = false;
        }
    }
    public void Init(Vector2Int tile, bool ineractive)
    {
        _interactive = ineractive;
        _tile = tile;
    }

    public void Init(Vector2Int tile, bool ineractive, Color color)
    {
        Init(tile, ineractive);
        _startColor = color;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (_interactive)
        {
            Select();
        }
    }

    public void Select()
    {
        _renderer.material.color = _selectedColor;
        OnTileSelected.Invoke(_tile);
    }

    private void OnDestroy()
    {
        OnTileSelected.RemoveAllListeners();
    }

    [Serializable]
    public class TileSelectedEvent : UnityEvent<Vector2Int> { }

}
