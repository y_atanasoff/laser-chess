﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionControl : MonoBehaviour
{
    [SerializeField]
    private float _waitTime;
    [SerializeField]
    private TileDrawer _drawer;
    private Vector2Int _selectedTile;

    public Vector2Int SelectedTile { get { return _selectedTile; } }

    public bool IsDone { get; private set; }

    public bool IsCanceled { get; set; }

    public float WaitTime {  get { return _waitTime; } }

    public void SelectTile(Vector2Int[] tiles, bool interactive = true)
    {
        IsDone = false;
        IsCanceled = false;
        StartCoroutine(SelectTileRoutine(tiles, interactive));
    }

    public void HighlightPosition(Vector2Int position)
    {
        _drawer.DrawTile(position, new Color(1, 1, 0, 0.5f));
    }
    public void Clear()
    {
        StopAllCoroutines();
        RemoveTiles();
    }

    private IEnumerator SelectTileRoutine(Vector2Int[] tiles, bool interactive)
    {
        _selectedTile = Vector2Int.one * -1;

        _drawer.DrawTiles(tiles, true, (Vector2Int t) => _selectedTile = t);

        yield return new WaitUntil(() => IsCanceled || (_selectedTile.x > -1 && _selectedTile.y > -1));
        yield return new WaitForSeconds(_waitTime);
        RemoveTiles();

        IsDone = true;
    }

    public void RemoveTiles()
    {
        _drawer.Clear();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            IsCanceled = true;
        }
    }
}
