﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour
{
    private void Start()
    {
        if (!LevelStore.Instance.IsFirstLevel)
        {
            gameObject.SetActive(false);
        }
    }

}
