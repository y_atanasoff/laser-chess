﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreen : MonoBehaviour {

    [SerializeField]
    private Text _text;
    [SerializeField]
    private Button _nextButton;
    public void ShowWinner(Player winner)
    {
        if (LevelStore.Instance.HasMoreLevels)
        {
            _text.text = winner.ToString() + " Player Wins!";
        }
        else
        {
            _text.text = "Congratulations! You Beat The Game!";
        }
        _nextButton.gameObject.SetActive(LevelStore.Instance.HasMoreLevels && winner == Player.Human);
        gameObject.SetActive(true);
    }


}
