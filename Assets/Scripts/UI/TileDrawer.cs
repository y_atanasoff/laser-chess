﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TileDrawer : MonoBehaviour
{
    [SerializeField]
    private GameObject _selectorPrefab;
    [SerializeField]
    private Map _map;

    private List<UISelector> _selectors = new List<UISelector>();

    public void DrawTiles(Vector2Int[] tiles, bool interactive = true, UnityAction<Vector2Int> action = null)
    {
        foreach (var tile in tiles)
        {
            var selector = Instantiate(_selectorPrefab,
                                       _map.MapTransform.LevelToMap(tile),
                                       Quaternion.identity, _map.MapTransform.MapParent).GetComponent<UISelector>();
            selector.Init(tile, interactive);
            selector.OnTileSelected.AddListener(action);
            _selectors.Add(selector);
        }
    }

    public void DrawTile(Vector2Int pos, Color color)
    {
        var selector = _selectors.Find(t => t.Tile == pos);
        if (selector == null)
        {
            selector = Instantiate(_selectorPrefab,
                           _map.MapTransform.LevelToMap(pos),
                           Quaternion.identity).GetComponent<UISelector>();
            selector.Init(pos, false, color);

            _selectors.Add(selector);
        }

    }

    public void Clear()
    {
        foreach (var selector in _selectors)
        {
            Destroy(selector.gameObject);
        }
        _selectors.Clear();
    }
}
