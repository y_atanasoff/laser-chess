﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{

    private bool _isRotating = false;
    public void RotateLeft()
    {
        StartCoroutine(RotateRoutine(90));
    }

    public void RotateRight()
    {
        StartCoroutine(RotateRoutine(-90));
        //transform.rotation *= Quaternion.AngleAxis(-90, Vector3.up);
    }

    private IEnumerator RotateRoutine(float angle)
    {
        if (_isRotating)
        {
            yield break;
        }

        _isRotating = true;
        var target = transform.rotation * Quaternion.AngleAxis(angle, Vector3.up);
        while (Quaternion.Angle(target, transform.rotation) > 1)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, target, 0.1f);
            yield return null;
        }
        transform.rotation = target;
        _isRotating = false;
    }
}
