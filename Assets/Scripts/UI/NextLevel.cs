﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour
{

    public void Next()
    {
        LevelStore.Instance.NextLevel();
        SceneManager.LoadScene(0);
    }
}
