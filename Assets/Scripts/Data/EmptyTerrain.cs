﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Descibes an empty terrain map
/// </summary>
[CreateAssetMenu(menuName = "Systems/EmptyTerrain")]
public class EmptyTerrain : ScriptableTerrain
{
    private RectInt _terrainRect;

    protected override void OnEnable()
    {
        _terrainRect = new RectInt(0, 0, _terrainSize.x, _terrainSize.y);
        base.OnEnable();
    }

    public override TerrainTile GetTile(Vector2Int position)
    {
        return new TerrainTile(TerrainType.Empty, IsPositionValid(position));
    }

    public override bool IsPositionValid(Vector2Int position)
    {
        return _terrainRect.Contains(position);
    }

    public override bool IsPositionInBounds(Vector2Int position)
    {
        return IsPositionValid(position);
    }
}
