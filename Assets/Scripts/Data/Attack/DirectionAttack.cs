﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// direction attack traces a ray in the direction of the selected position and attacks the first hit unit
/// </summary>
[CreateAssetMenu(menuName ="Attack/Direction")]
public class DirectionAttack : ScriptableAttack
{
    [SerializeField]
    private bool _hitAllUnits = false;
    [SerializeField]
    private bool _friendlyFire = false;

    public override ScriptableUnit[] Attack(ScriptableGrid grid, ScriptableUnit unit, int damage, Vector2Int position)
    {
        List<ScriptableUnit> attackedUnits = new List<ScriptableUnit>();
        Vector2Int dir = position - unit.Position;
        dir.x = dir.x == 0 ? 0 : (int)Mathf.Sign(dir.x);
        dir.y = dir.y == 0 ? 0 : (int)Mathf.Sign(dir.y);

        Vector2Int pos = unit.Position + dir;
        ScriptableUnit otherUnit = null;
        while (grid.Terrain.IsPositionInBounds(pos))
        {
            if (grid.GetUnit(pos, out otherUnit))
            {
                if (otherUnit.Player != unit.Player || _friendlyFire)
                {
                    otherUnit.TakeDamage(damage);
                    attackedUnits.Add(otherUnit);
                }
                if (!_hitAllUnits)
                {
                    break;
                }
            }
            pos += dir;
        }
        return attackedUnits.ToArray();
    }
}
