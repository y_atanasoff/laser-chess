﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// hits all units within the possible tiles
/// </summary>
[CreateAssetMenu(menuName ="Attack/HitAll")]
public class HitAllAttack : ScriptableAttack
{
    [SerializeField]
    private bool _friendlyFire;

    public override ScriptableUnit[] Attack(ScriptableGrid grid, ScriptableUnit unit, int damage, Vector2Int position)
    {
        List<ScriptableUnit> attackedUnits = new List<ScriptableUnit>();
        ScriptableUnit otherUnit = null;
        var tiles = SelectTiles(grid, unit);
        foreach (var tile in tiles)
        {
            if (grid.GetUnit(tile, out otherUnit))
            {
                if (otherUnit.Player != unit.Player || _friendlyFire)
                {
                    otherUnit.TakeDamage(damage);
                    attackedUnits.Add(otherUnit);
                }
            }
        }
        return attackedUnits.ToArray();
    }
}
