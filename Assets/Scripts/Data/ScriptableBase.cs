﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for all scriptable data in the game
/// </summary>
[Serializable]
public class ScriptableBase : ScriptableObject
{
    [SerializeField]
    protected HideFlags _hideFlags;

    protected virtual void OnEnable()
    {
        //Debug.Log("Enabled: " + name);
        this.hideFlags = _hideFlags;
    }

    /// <summary>
    /// Set the current HideFlags and keep them after OnEnable has been called
    /// </summary>
    public void SetHideFlags(HideFlags flags)
    {
        _hideFlags = flags;
        hideFlags = flags;
    }
}