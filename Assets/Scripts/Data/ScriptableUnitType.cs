﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Units/UnitType")]
public class ScriptableUnitType : ScriptableBase
{
    public override bool Equals(object other)
    {
        var otherUnitType = other as ScriptableUnitType;
        if (otherUnitType != null)
        {
            return name.Equals(otherUnitType.name);
        }
        return base.Equals(other);
    }

    public override int GetHashCode()
    {
        return name.GetHashCode();
    }
}
