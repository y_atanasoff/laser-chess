﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Descibes map dimensions and possible terrain in the game.
/// </summary>
[Serializable]
public class ScriptableTerrain : ScriptableBase
{
    [SerializeField]
    protected Vector2Int _terrainSize;

    /// <summary>
    /// Size of the map. All tiles occupy the space between (0,0) and (Size.x, Size.y)
    /// </summary>
    public Vector2Int Size { get { return _terrainSize; } }

    /// <summary>
    /// Gets a tile from the terrain
    /// </summary>
    public virtual TerrainTile GetTile(Vector2Int position) { return null; }

    /// <summary>
    /// Is this a position a unit can move to?
    /// </summary>
    public virtual bool IsPositionValid(Vector2Int position) { return false; }

    /// <summary>
    /// Is this a position a unit can move to?
    /// </summary>
    public virtual bool IsPositionInBounds(Vector2Int position) { return false; }
}
