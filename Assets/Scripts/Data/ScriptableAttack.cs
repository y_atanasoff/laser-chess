﻿using UnityEngine;

/// <summary>
/// Base class that describes how a unit can attack and which tiles it can attack
/// </summary>
public class ScriptableAttack : ScriptableBase
{
    [SerializeField]
    private ScriptableSelector _tileSelector;
    /// <summary>
    /// selects tiles that the unit can possibly attack
    /// </summary>
    public virtual Vector2Int[] SelectTiles(ScriptableGrid grid, ScriptableUnit unit)
    {
        return _tileSelector.SelectTiles(grid, unit, true, false);
    }

    /// <summary>
    /// executes the attack
    /// the actual hit units can be unrelated to the attack position
    /// </summary>
    public virtual ScriptableUnit[] Attack(ScriptableGrid grid, ScriptableUnit unit, int damage, Vector2Int position)
    {
        return new ScriptableUnit[0];
    }
}
