﻿using System;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Base class for all unit data
/// </summary>
[Serializable]
[CreateAssetMenu(menuName = "Units/Unit")]
public class ScriptableUnit : ScriptableBase
{

    [SerializeField]
    private int _priority;
    [SerializeField]
    private Vector2Int _position;
    [SerializeField]
    private Player _player;
    [SerializeField]
    private int _health;
    [SerializeField]
    private int _damage;
    [SerializeField]
    private  ScriptableSelector _moveSelector;
    [SerializeField]
    private ScriptableAttack _attack;
    [SerializeField]
    private ScriptableUnitType _unitType;
    [SerializeField]
    private ScriptableLogic _moveLogic;
    [SerializeField]
    private ScriptableLogic _attackLogic;

    private ScriptableGrid _grid;

    public Vector2Int Forward { get { return _player == Player.Human ? Vector2Int.up : Vector2Int.down; } }

    public int Health { get { return _health; } }
    public int Damage { get { return _damage; } }
    public int Priority { get { return _priority; } }
    public bool CanAttack { get { return _attack != null; } }
    public bool CanMove { get { return _moveSelector != null; } }

    public Player Player { get { return _player; } }
    public Vector2Int Position { get { return _position; } }

    public ScriptableLogic MoveLogic { get { return _moveLogic; } }
    public ScriptableLogic AttackLogic { get { return _attackLogic; } }
    public ScriptableUnitType UnitType { get { return _unitType; } }
    public ScriptableGrid Grid { get { return _grid; } }

    public UnitMoveEvent OnUnitMoved;
    public UnitAttackEvent OnUnitAttack;

    public void Initialize(ScriptableGrid grid, Vector2Int position, Player player)
    {
        _grid = grid;
        _position = position;
        _player = player;
    }

    public bool Move(Vector2Int position)
    {
        if (_grid.Terrain.IsPositionValid(position))
        {
            _position = position;
            OnUnitMoved.Invoke(position);
            return true;
        }
        return false;
    }

    public void Attack(Vector2Int position)
    {
        if (_attack != null)
        {
            var attackedUnits = _attack.Attack(_grid, this, _damage, position);
            OnUnitAttack.Invoke(_attack.SelectTiles(_grid, this), position, attackedUnits);
        }
    }

    public void TakeDamage(int damage)
    {
        _health -= damage;
    }

    public Vector2Int[] PossibleMove()
    {
        return _moveSelector.SelectTiles(_grid, this, false, true);
    }

    public Vector2Int[] PossibleAttack()
    {
        if (_attack != null)
        {
            return _attack.SelectTiles(_grid, this);
        }
        return new Vector2Int[0];
    }

    public void OnDestroy()
    {
        OnUnitAttack.RemoveAllListeners();
        OnUnitMoved.RemoveAllListeners();
    }

    [Serializable]
    public class UnitMoveEvent : UnityEvent<Vector2Int> { }
    [Serializable]
    public class UnitAttackEvent : UnityEvent<Vector2Int[], Vector2Int, ScriptableUnit[]> { }
    [Serializable]
    public class UnitDamageEvent : UnityEvent<int, bool> { }
    [Serializable]
    public class UnitKilledEvent : UnityEvent { }

}