﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
/// <summary>
/// Avoids potential danger.
/// </summary>
[CreateAssetMenu(menuName = "Logic/MoveFromDanger")]
public class MoveFromDangerLogic : ScriptableLogic
{
    [SerializeField]
    private float _enemyWeight;
    [SerializeField]
    private float _fiendWeight;
    [SerializeField]
    private bool _favourCentralTiles;
    [SerializeField]
    private float _centralTileWeight;
    [SerializeField]
    private float _attackedTileWeight;
    [SerializeField]
    private float _occupiedTileWeight;
    [SerializeField]
    private float _maxEnemyDistance;

    private readonly Vector2Int[] _directions =
    {
        new Vector2Int(-1, -1),
        new Vector2Int(-1, 0),
        new Vector2Int(-1, 1),
        new Vector2Int(0, 1),
        new Vector2Int(1, 1),
        new Vector2Int(1, -1),
        new Vector2Int(0, -1),
        new Vector2Int(1, 0)
    };

    public override void CalculateMove(ScriptableUnit unit, ScriptableGrid level)
    {
#if DEBUG_AI
        InitDebug();
        ClearDebug();
#endif
        //initialize move weights
        Success = true;

        var possibleMove = new List<Vector2Int>(unit.PossibleMove());

        var moveWeights = new Dictionary<Vector2Int, float>();
        foreach (var move in possibleMove)
        {
            moveWeights.Add(move, CalculateMoveWeight(move, unit, level));
        }
        //if we favour central tiles the unit will try to stay in the center 
        //insted of getting bogged down in the corners
        if (_favourCentralTiles)
        {
            int center = Mathf.CeilToInt(level.Terrain.Size.x * 0.5f);

            foreach (var move in possibleMove)
            {
                moveWeights[move] += (1.0f - (Mathf.Abs(center - move.x) / (float)center)) * _centralTileWeight;
            }
        }

        var enemies = level.GetUnits(PlayerUtils.Opposite(unit.Player));
        var attackedTiles = new List<Vector2Int>();
        foreach (var enemy in enemies)
        {
            attackedTiles.AddRange(enemy.PossibleAttack());
        }
        attackedTiles = attackedTiles.Distinct().ToList();
        var enemyTiles = enemies.Select(e => e.Position).ToList();

        var furthestFromAttack = GetTileFurthestFromDanger(attackedTiles, possibleMove);
        if (enemyTiles.Select(t => Vector2Int.Distance(t, unit.Position)).Min() < _maxEnemyDistance)
        {
            var furthestFromEnemy = GetTileFurthestFromDanger(enemyTiles, possibleMove);
            moveWeights[furthestFromEnemy] += _occupiedTileWeight;
#if DEBUG_AI
            DrawTile(furthestFromEnemy, new Color(1, 1, 0, 0.5f));
#endif

        }

#if DEBUG_AI
        DrawTile(furthestFromAttack, new Color(1, 0, 0, 0.5f));
        
#endif
        moveWeights[furthestFromAttack] += _attackedTileWeight;
        

        //find the best weighted possible tile
        Move = unit.Position;
        float bestWeight = float.MinValue;
        foreach (var tile in moveWeights)
        {
            if (tile.Value > bestWeight)
            {
                Move = tile.Key;
                bestWeight = tile.Value;
            }
        }

#if DEBUG_AI
        DrawWeights(moveWeights);
#endif

    }
    private Vector2Int GetTileFurthestFromDanger(List<Vector2Int> dangerTiles, List<Vector2Int> possibleMove)
    {
        var minDistances = new List<float>();
        for (int i = 0; i < possibleMove.Count; i++)
        {

#if DEBUG_AI
            foreach (var tile in dangerTiles)
            {
                DrawTile(tile, new Color(Vector2Int.Distance(tile, possibleMove[i]) / 8.0f, 0, 0, 1));
            }
#endif
            minDistances.Add(dangerTiles.Select(t => Vector2Int.Distance(t, possibleMove[i])).Min());
            Debug.LogFormat("Minimum Distance From Dange For Tile {0} - {1}", possibleMove[i], minDistances.Last());
        }
        return possibleMove[minDistances.IndexOf(minDistances.Max())];
    }


    private float CalculateMoveWeight(Vector2Int pos, ScriptableUnit unit, ScriptableGrid level)
    {
        var units = new List<ScriptableUnit>();

        units.AddRange(level.GetUnits(Player.AI));
        units.AddRange(level.GetUnits(Player.Human));
        units.Remove(unit);
        float weight = 0;
        ScriptableUnit outUnit;
        //for each direction we trace a ray and find the first unit that can hit us
        //we adjust the the tile weight based on whether the unit is friendly or an enemy
        foreach (var dir in _directions)
        {
            if (HasUnit(pos, dir, level.Terrain, units, out outUnit))
            {
#if DEBUG_AI
                DrawTile(outUnit.Position, new Color(1,1,0,0.5f) );
#endif
                if (outUnit.Player != unit.Player)
                {
                    if (Array.FindIndex(outUnit.PossibleAttack(), t => t == pos) >= 0)
                    {
                        weight -= _enemyWeight;
                    }
                }
                else
                {
                    //friendly units can provide shelter
                    weight += _fiendWeight;
                }
            }
        }
//#if DEBUG_AI
//        Debug.LogFormat("Tile {0} has a weight of {1} ", pos, weight);
//#endif
        return weight;
    }

    private bool HasUnit(   Vector2Int pos, 
                            Vector2Int dir, 
                            ScriptableTerrain terrain, 
                            List<ScriptableUnit> units, 
                            out ScriptableUnit outUnit)
    {

        outUnit = null;
        do
        {
            pos += dir;
            outUnit = units.Find(u => u.Position == pos);
        } while (terrain.IsPositionInBounds(pos) && outUnit == null);

        return outUnit != null;
    }
}

