﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;
/// <summary>
/// Searches for the closest enemy to attack
/// </summary>
[CreateAssetMenu(menuName = "Logic/MoveClosest")]
public class MoveToClosestEnemyLogic : ScriptableLogic
{
    [SerializeField]
    private bool _debug;
    [SerializeField]
    private float _friendWeight;
    [SerializeField]
    private float _enemyWeight;
    [SerializeField]
    private float _closestWeight;

    public override void CalculateMove(ScriptableUnit unit, ScriptableGrid level)
    {
        var enemyUnits = new List<ScriptableUnit>(level.GetUnits(PlayerUtils.Opposite(unit.Player)));
        var possibleMoves = new List<Vector2Int>(unit.PossibleMove());
        Success = possibleMoves.Count > 0 && enemyUnits.Count > 0;

        if (!Success)
        {
            return;
        }

        var moveWeights = InitWeights(possibleMoves.ToArray());
        var friendlyUnits = new List<ScriptableUnit>(level.GetUnits(unit.Player));
        friendlyUnits.Remove(unit);

        //each possible tile is weighted based on how many enemies we can attakc from it
        foreach (var tile in possibleMoves)
        {
            moveWeights[tile] = CalculateWeight(tile, unit, enemyUnits, friendlyUnits);
        }
        //the closest tile gets a bonus weight
        var closestTile = TileClosestToEnemy(possibleMoves.ToArray(), enemyUnits);
        moveWeights[closestTile] += _closestWeight;

        Move = unit.Position;
        float bestWeight = 0;
        foreach (var tile in moveWeights)
        {
            if (tile.Value > bestWeight)
            {
                Move = tile.Key;
                bestWeight = tile.Value;
            }
        }

#if DEBUG_AI
        Debug.LogFormat("Tile Closest To Enemy Is {0}", closestTile);
        DrawWeights(moveWeights);
#endif

    }

    private float CalculateWeight(Vector2Int tile, ScriptableUnit unit, List<ScriptableUnit> enemyUnits, List<ScriptableUnit> friendlyUnits)
    {
        Vector2Int offset = tile - unit.Position;
        Vector2Int[] attack = unit.PossibleAttack();
        int friends = 0;
        //find all the enemies we can possibly attack from this tile
        int enemies = attack.Select(t => t + offset).
                             Where(t => enemyUnits.FindIndex(e => e.Position == t) >= 0).
                             Count();

        //if there are enemies, we must check for possible friendly fire
        //we only check if we intend to attack, otherwise it might skew the weight
        if (enemies > 0)
        {
            friends = attack.Select(t => t + offset).
                             Where(t => friendlyUnits.FindIndex(e => e.Position == t) >= 0).
                             Count();
        }

#if DEBUG_AI
        Debug.LogFormat("Can Attack {0} enemies and {1} friends from Tile {2}", enemies, friends, tile);
#endif
        return enemies * _enemyWeight - friends * _friendWeight;
    }


    private Dictionary<Vector2Int, float> InitWeights(Vector2Int[] moves)
    {
        var moveWeights = new Dictionary<Vector2Int, float>();
        foreach (var move in moves)
        {
            moveWeights.Add(move, 0);
        }
        return moveWeights;
    }

    private Vector2Int TileClosestToEnemy(Vector2Int[] possibleMoves, List<ScriptableUnit> enemyUnits)
    {
        float minDistance = float.MaxValue;
        Vector2Int closest = possibleMoves[0];

        foreach (var enUnit in enemyUnits)
        {
            foreach (var tile in possibleMoves)
            {
                float distance = MoveDistance(enUnit.Position, tile);
                if (distance < minDistance)
                {
                    closest = tile;
                    minDistance = distance;
                }
            }
        }
        return closest;
    }

    private float MoveDistance(Vector2Int a, Vector2Int b)
    {
        return Mathf.Max(Mathf.Abs(a.x - b.x), Mathf.Abs(a.y - b.y));
    }
}
