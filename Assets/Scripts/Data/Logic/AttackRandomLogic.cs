﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// An attack behaviour that finds the nearest enemy to attack
/// </summary>
[CreateAssetMenu(menuName = "Logic/RandomAttack")]
public class AttackRandomLogic : ScriptableLogic
{
    public override void CalculateMove(ScriptableUnit unit, ScriptableGrid level)
    {
        var enemyUnits = new List<ScriptableUnit>(level.GetUnits(PlayerUtils.Opposite(unit.Player)));
        List<Vector2Int> possibleMoves = new List<Vector2Int>(unit.PossibleAttack());
        possibleMoves.Add(unit.Position);
        var possibleEnemies = enemyUnits.FindAll(e => possibleMoves.Contains(e.Position));
        Success = possibleEnemies.Count > 0;

        if (Success)
        {
            Move = possibleEnemies[Random.Range(0, possibleEnemies.Count)].Position;
        }

    }
}