﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Base class for describing unit behaviour
/// </summary>
public class ScriptableLogic : ScriptableBase
{
    /// <summary>
    /// The calculated move
    /// </summary>
    public Vector2Int Move { get; protected set; }

    /// <summary>
    /// Did we find a possible move
    /// </summary>
    public bool Success { get; protected set; }

    /// <summary>
    /// Calculates the unit's next move
    /// </summary>
    /// <param name="unit">the unit whose move we need to calculate</param>
    /// <param name="level">the game level</param>
    public virtual void CalculateMove(ScriptableUnit unit, ScriptableGrid level)
    {
    }
#if DEBUG_AI
    private TileDrawer _debugDrawer;

    protected void InitDebug()
    {
        if (_debugDrawer == null)
        {
            _debugDrawer = GameObject.FindGameObjectWithTag("DebugDrawer").GetComponent<TileDrawer>();
            if (_debugDrawer != null)
            {
                Debug.Log("Aquired Debug Drawer");
            }
        }

    }

    protected void DrawWeights(Dictionary<Vector2Int, float> weights)
    {
        if (_debugDrawer != null)
        {

            float maxWeight = weights.Max(w => w.Value);
            foreach (var tile in weights)
            {
                Debug.LogFormat("Weight For Tile {0} - {1}", tile.Key, tile.Value);
                _debugDrawer.DrawTile(tile.Key, Color.blue * Mathf.Lerp(.2f, .9f, (tile.Value / maxWeight)));
            }
        }
    }

    protected void DrawTile(Vector2Int tile, Color color)
    {
        if (_debugDrawer != null)
        {
            _debugDrawer.DrawTile(tile, color);
        }
    }
    protected void ClearDebug()
    {
        if (_debugDrawer != null)
        {
            _debugDrawer.Clear();
        }
    }
#endif


}
