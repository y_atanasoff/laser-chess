﻿using UnityEngine;
/// <summary>
/// Moves forward by one square
/// </summary>
[CreateAssetMenu(menuName = "Logic/MoveForward")]
public class MoveForwardLogic : ScriptableLogic
{
    public override void CalculateMove(ScriptableUnit unit, ScriptableGrid level)
    {
        Move = unit.Position + unit.Forward;
        Success = System.Array.FindIndex(unit.PossibleMove(), t => t == Move) >= 0;
    }
}
