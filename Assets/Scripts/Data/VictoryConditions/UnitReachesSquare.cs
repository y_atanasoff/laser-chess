﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Victory/UnitReachesSquare")]

public class UnitReachesSquare : VictoryCondition
{
    [SerializeField]
    private ScriptableUnitType _unitType;
    [SerializeField]
    private int Square;

    public override bool IsValid(ScriptableGrid grid)
    {
        var units = grid.GetUnits(_player);
        return System.Array.FindIndex(units, u => u.UnitType == _unitType && u.Position.y == Square) >= 0;
    }
}
