﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Victory/DestroyEnemyUnits")]

public class DestroyEnemyUnits : VictoryCondition
{
    public override bool IsValid(ScriptableGrid grid)
    {
        return grid.GetUnits(PlayerUtils.Opposite(_player)).Length == 0;
    }
}
