﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Victory/DestroyUnitsOfType")]

public class DestroyUnitsOfType : VictoryCondition
{
    [SerializeField]
    private ScriptableUnitType _unitType;

    public override bool IsValid(ScriptableGrid grid)
    {
        var units = grid.GetUnits(PlayerUtils.Opposite(_player));
        return System.Array.FindAll(units, u => u.UnitType == _unitType).Length == 0;
    }
}
