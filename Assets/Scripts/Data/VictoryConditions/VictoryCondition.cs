﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for scripts that determine which player wins
/// </summary>
public class VictoryCondition : ScriptableBase
{
    [SerializeField]
    protected Player _player;

    public Player player { get { return _player; } }

    public virtual bool IsValid(ScriptableGrid grid)
    {
        return false;
    }
}
