﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Selectors/Orthogonal")]
public class TileSelector : ScriptableSelector 
{
    [SerializeField]
    private Vector2Int[] _offsets;
    [SerializeField]
    private int _range;

    private void OnEnable()
    {
        _range = _range == -1 ? int.MaxValue : _range;
    }

    public override Vector2Int[] SelectTiles(ScriptableGrid grid, ScriptableUnit unit, bool includeUnits, bool includeStart)
    {
        List<Vector2Int> tiles = new List<Vector2Int>();
        if (includeStart)
        {
            tiles.Add(unit.Position);
        }
        foreach (var offset in _offsets)
        {
            int distance = 0;
            var position = unit.Position;
            do
            {
                position += offset;
                if (grid.Terrain.IsPositionValid(position) && (includeUnits || !grid.HasUnit(position)))
                {
                    tiles.Add(position);
                }
            }
            while (grid.Terrain.IsPositionInBounds(position) && !grid.HasUnit(position) && (++distance < _range)) ;
        }
        return tiles.ToArray();
    }
}
