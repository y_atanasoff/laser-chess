﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for scipts that filter the grid based on different conditions
/// </summary>
/// 
public class ScriptableSelector : ScriptableObject
{
    public virtual Vector2Int[] SelectTiles(ScriptableGrid grid, ScriptableUnit unit, bool includeUnits, bool includeStart)
    {
        return new Vector2Int[0];
    }
}