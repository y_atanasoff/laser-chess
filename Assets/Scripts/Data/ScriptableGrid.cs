﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Data class that holds the model for the game.
/// </summary>
[CreateAssetMenu(menuName = "Systems/Grid")]
public class ScriptableGrid : ScriptableBase
{
    //todo:move to a more appropriate place
    [Serializable]
    public class StartingUnit
    {
        public ScriptableUnit Unit;
        public Vector2Int Position;
    }

    //prototype units for the level
    [SerializeField]
    private StartingUnit[] _aiStartingUnits;
    [SerializeField]
    private StartingUnit[] _humanStartingUnits;

    [SerializeField]
    private ScriptableTerrain _terrain;
    [SerializeField]
    private VictoryCondition[] _victoryConditions;

    private List<ScriptableUnit> _units;

    /// <summary>
    /// The map terrain
    /// </summary>
    public ScriptableTerrain Terrain { get { return _terrain; } }

    protected override void OnEnable()
    {
        base.OnEnable();
        //we're just starting the level - we have an empty grid and we want to fill it with units
        Reset();
    }
    /// <summary>
    /// recreate the contents of the grid
    /// </summary>
    public void Reset()
    {
        _units = LoadStartingUnits(_aiStartingUnits, Player.AI);
        _units.AddRange(LoadStartingUnits(_humanStartingUnits, Player.Human));
    }


    /// <summary>
    /// Gets the unit models in the game based on PlayerType
    /// </summary>
    public ScriptableUnit[] GetUnits(Player player)
    {
        return _units.FindAll(u => u.Player == player).ToArray();
    }


    /// <summary>
    /// returns a unit based on player type
    /// </summary>
    public bool GetPlayerUnit(Vector2Int position, Player player, out ScriptableUnit unit)
    {
        unit = _units.Find(u => u.Position == position && u.Player == player);
        return unit != null;
    }

    /// <summary>
    /// returns if there is a unit regardles of the player type
    /// </summary>
    public bool GetUnit(Vector2Int position, out ScriptableUnit unit)
    {
        unit = _units.Find(u => u.Position == position);
        return unit != null;
    }

    /// <summary>
    /// removes all dead units from the grid and the game
    /// </summary>
    public void ClearDeadUnits()
    {
        Predicate<ScriptableUnit> deadPredicate = u => u.Health <= 0;
        var deadUnits = _units.FindAll(deadPredicate).ToArray();
        _units.RemoveAll(deadPredicate);
        foreach (var unit in deadUnits)
        {
            Destroy(unit);
        }
    }
    /// <summary>
    /// is there a unit on this position?
    /// </summary>
    public bool HasUnit(Vector2Int position)
    {
        return _units.FindIndex(u => u.Position == position) >= 0;
    }

    /// <summary>
    /// check victory conditions
    /// </summary>
    public bool PlayerWins(ref Player player)
    {
        foreach (var condition in _victoryConditions)
        {
            if (condition.IsValid(this))
            {
                player = condition.player;
                return true;
            }
        }
        return false;
    }

    private List<ScriptableUnit> LoadStartingUnits(StartingUnit[] startingUnits, Player player)
    {
        var list = new List<ScriptableUnit>();
        if (startingUnits != null)
        {
            foreach (var unit in startingUnits)
            {
                //copy the prototype unit and fill in some of the data
                var new_unit = Instantiate(unit.Unit);
                new_unit.Initialize(this, unit.Position, player);
                new_unit.SetHideFlags(HideFlags.HideAndDontSave);
                list.Add(new_unit);
            }
        }
        return list;
    }
}
