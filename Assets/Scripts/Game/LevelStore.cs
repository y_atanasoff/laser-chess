﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// stores all the possible levels for the game
/// </summary>
public class LevelStore : MonoBehaviour
{
    private static LevelStore _instance = null;
    public static LevelStore Instance { get { return _instance; } set { _instance = value; } }

    [SerializeField]
    private ScriptableGrid[] _levels;

    private int _currentLevelIndex = 0;

    public bool HasMoreLevels { get { return _currentLevelIndex < _levels.Length - 1; } }

    public bool IsFirstLevel { get { return _currentLevelIndex == 0; } }

    public ScriptableGrid CurrentLevel { get { return _levels[_currentLevelIndex]; } }


    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void NextLevel()
    {
        _currentLevelIndex = Mathf.Min(_levels.Length, _currentLevelIndex + 1);
    }

}
