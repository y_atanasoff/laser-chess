﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Player { AI = 0, Human };

public static class PlayerUtils
{
    public static Player Opposite(Player player)
    {
        return player == Player.AI ? Player.Human : Player.AI;
    }
}

public enum TerrainType { Empty = 0 }

[Serializable]
public class TerrainTile
{
    [SerializeField]
    private bool _isValid;
    [SerializeField]
    private TerrainType _type;

    public bool IsValid { get { return IsValid; } }
    public TerrainType Type { get { return _type; } }
    public TerrainTile(TerrainType type, bool valid)
    {
        _isValid = valid;
        _type = type;
    }

}
