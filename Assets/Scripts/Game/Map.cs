﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// View (in-game) part of the Level. Draws the level and controls in-game actions
/// </summary>
public class Map : MonoBehaviour
{
    [SerializeField]
    private ScriptableGrid _level;
    [SerializeField]
    private Player _startingPlayer;

    private IGridFactory _gridBuilder;
    private IUnitFactory _unitFactory;
    private Player _currentPlayer;

    private Dictionary<Player, TurnController> _turnControllers = new Dictionary<Player, TurnController>();
    private Dictionary<ScriptableUnit, GameObject> _views = new Dictionary<ScriptableUnit, GameObject>();

    private bool _isPaused = false;
    public bool isPaused
    {
        get { return _isPaused = false; }
        set
        {
            _isPaused = value;
            foreach (var tc in _turnControllers)
            {
                tc.Value.isPaused = value;
            }
        }
    }

    public MapTransform MapTransform { get; private set;}
    public PlayerWinEvent OnPlayerWin;

    private void Start()
    {
        _level = LevelStore.Instance.CurrentLevel;
        _level.Reset();

        MapTransform = new MapTransform(_level.Terrain, transform);

        _gridBuilder = (IGridFactory)GetComponent(typeof(IGridFactory));
        _unitFactory = (IUnitFactory)GetComponent(typeof(IUnitFactory));

        if (_gridBuilder == null || _unitFactory == null)
        {
            Debug.LogError("Missing Essential Components For Building The Level. Stopping");
            return;
        }

        _gridBuilder.BuildGrid(_level, this);

        foreach (var turnController in GetComponentsInChildren<TurnController>())
        {
           _turnControllers.Add(turnController.player, turnController);
        }

        foreach (var unit in _level.GetUnits(Player.AI))
        {
            _views.Add(unit, _unitFactory.CreateUnit(unit, this));
        }

        foreach (var unit in _level.GetUnits(Player.Human))
        {
            _views.Add(unit, _unitFactory.CreateUnit(unit, this));
        }

        StartCoroutine(GameLoop());
    }

    public GameObject GetView(ScriptableUnit scrUnit)
    {
        Debug.Log("Getting view for " + scrUnit.UnitType.name);
        return _views[scrUnit];
    }

    private IEnumerator GameLoop()
    {
        _currentPlayer = _startingPlayer;
        var turn = _turnControllers[_currentPlayer];
        turn.StartTurn(_level.GetUnits(_currentPlayer));
        Player winner = Player.Human;
        do
        {
            if (turn.IsOver)
            {
                _currentPlayer = PlayerUtils.Opposite(_currentPlayer);
                turn = _turnControllers[_currentPlayer];
                turn.StartTurn(_level.GetUnits(_currentPlayer));
            }
            Debug.Log("Making turn for " + _currentPlayer.ToString());
            turn.MakeTurn();
            yield return new WaitWhile(() => turn.IsMakingMove || isPaused);
            _level.ClearDeadUnits();
        } while (!(_level.PlayerWins(ref winner)));

        OnPlayerWin.Invoke(winner);
    }

    [Serializable]
    public class PlayerWinEvent : UnityEvent<Player> { }
}
