﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Selector base on unit priority (defined in ScriptableUnit).
/// </summary>
public class PrioritySelector : ISelector
{
    public bool HasMoreMoves
    {
        get
        {
            return _units.Count > 0;
        }
    }

    public Queue<ScriptableUnit> _units = new Queue<ScriptableUnit>();

    public void ResetSelector(ScriptableUnit[] units)
    {
        Array.Sort(units, (a, b) => a.Priority.CompareTo(b.Priority));
        foreach (var unit in units)
        {
            _units.Enqueue(unit);
        }
    }

    public ScriptableUnit SelectNextUnit()
    {
        return _units.Count > 0 ? _units.Dequeue() : null;
    }
}
