﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface for a class that create in-game units
/// </summary>
public interface IUnitFactory
{
    GameObject CreateUnit(ScriptableUnit unit, Map map);
}
