﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UnitFactory : MonoBehaviour, IUnitFactory
{
    [System.Serializable]
    public class UnitBlueprint
    {
        public ScriptableUnitType UnitType;
        public GameObject Prefab;
    }

    [SerializeField]
    private GameObject _unitPrefab;
    [SerializeField]
    private UnitBlueprint[] _unitBlueprints;

    private Dictionary<ScriptableUnitType, GameObject> _blueprints = new Dictionary<ScriptableUnitType, GameObject>();
    private void Awake()
    {
        foreach (var bp in _unitBlueprints)
        {
            _blueprints.Add(bp.UnitType, bp.Prefab);
        }
    }


    public GameObject CreateUnit(ScriptableUnit unit, Map map)
    {
        var go = Instantiate(_blueprints[unit.UnitType], 
                            map.MapTransform.LevelToMap(unit.Position),
                            map.MapTransform.Direction(unit.Player), 
                            map.MapTransform.MapParent);
        go.GetComponent<Unit>().Init(map, unit);
        return go;
    }
}
