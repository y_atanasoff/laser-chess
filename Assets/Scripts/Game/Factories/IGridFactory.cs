﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface for a class that creates the grid game object(s)
/// </summary>
public interface IGridFactory
{
    void BuildGrid(ScriptableGrid grid, Map map);
}
