﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// creates selectors that can be used to choose the next unit
/// </summary>
public enum SelectionType { Priority }
public static class SelectorFactotry
{
    public static ISelector CreateSelector(SelectionType type)
    {
        switch (type)
        {
            case SelectionType.Priority:
                return new PrioritySelector();
        }

        throw new NotImplementedException();
    }
}
