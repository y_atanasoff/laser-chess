﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// interface for defining a selector
/// a selector allows for different strategies by which to choose the next unit
/// </summary>
public interface ISelector
{
    bool HasMoreMoves { get; }

    void ResetSelector(ScriptableUnit[] units);

    ScriptableUnit SelectNextUnit();
}
