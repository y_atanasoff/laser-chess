﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Handles map transformations between world / map space and level space
/// </summary>
public class MapTransform
{
    private Matrix4x4 _tMatrix;
    private Transform _parent;
    /// <summary>
    /// The transform that holds the entire map
    /// </summary>
    public Transform MapParent { get { return _parent; } }

    /// <summary>
    /// Connstructor
    /// </summary>
    public MapTransform(ScriptableTerrain terrain, Transform parent)
    {
        _parent = parent;
        var rotation = Quaternion.AngleAxis(90, Vector3.right);
        var offset = rotation * (-0.5f * (Vector2)(terrain.Size - Vector2Int.one));
        _tMatrix = Matrix4x4.TRS(offset, rotation, Vector3.one);
    }

    /// <summary>
    /// Transforms a level position into a position local to the map parent
    /// </summary>
    public Vector3 LevelToMap(Vector2Int position)
    {
        return _tMatrix * new Vector4(position.x, position.y, 0, 1);
    }
    /// <summary>
    /// Transforms a level position to a world position
    /// </summary>
    public Vector3 LevelToWorld(Vector2Int position)
    {
        return _parent.TransformPoint(LevelToMap(position));
    }

    /// <summary>
    /// Transforms a level position local to the map parent into a level position
    /// </summary>
    public Vector2Int MapToLevel(Vector3 position)
    {
        var newpos = _tMatrix.inverse * new Vector4(position.x, position.y, position.z, 1);
        return new Vector2Int((int)newpos.x, (int)newpos.y);
    }
    /// <summary>
    /// Transforms a world position to a level position
    /// </summary>
    public Vector2Int WorldToLevel(Vector3 position)
    {
        return MapToLevel(_parent.InverseTransformPoint(position));
    }

    /// <summary>
    /// The rotation which units/ objects should use depending on their player
    /// </summary>
    public Quaternion Direction(Player player)
    {
        return player == Player.Human ? Quaternion.identity : Quaternion.AngleAxis(180, Vector3.up);
    }


}
