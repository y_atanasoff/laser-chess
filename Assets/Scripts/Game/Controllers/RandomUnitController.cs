﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomUnitController : MonoBehaviour, IUnitContoller
{
    public bool IsDone { get; private set; }

    public void Attack(ScriptableUnit unit)
    {
        StartCoroutine(AttackRoutine(unit));
    }

    public void MoveUnit(ScriptableUnit unit)
    {
        StartCoroutine(MoveRoutine(unit));
    }

    private IEnumerator MoveRoutine(ScriptableUnit unit)
    {
        IsDone = false;
        var move = unit.PossibleMove();
        yield return new WaitForSeconds(1.0f);
        if (move != null && move.Length > 0)
        {
            var tile = move[UnityEngine.Random.Range(0, move.Length)];
            unit.Move(tile);
        }
        IsDone = true;
    }

    private IEnumerator AttackRoutine(ScriptableUnit unit)
    {
        IsDone = false;
        if (unit.CanAttack)
        {
            var attack = unit.PossibleAttack();

            yield return new WaitForSeconds(1.0f);
            if (attack != null && attack.Length > 0)
            {
                var tile = attack[UnityEngine.Random.Range(0, attack.Length)];
                unit.Attack(tile);
            }
        }
        IsDone = true;
    }

    public void Clear()
    {
        StopAllCoroutines();
    }
}
