﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AILogicController : MonoBehaviour, IUnitContoller
{
    public bool IsDone { get; private set; }
    [SerializeField]
    private float _actionPause;
    public void Attack(ScriptableUnit unit)
    {
        StartCoroutine(AttackRoutine(unit));
    }

    public void MoveUnit(ScriptableUnit unit)
    {
        StartCoroutine(MoveRoutine(unit));
    }

    private IEnumerator MoveRoutine(ScriptableUnit unit)
    {
        IsDone = false;
        if (unit.CanMove)
        {
            var move = unit.PossibleMove();
            unit.MoveLogic.CalculateMove(unit, unit.Grid);
            yield return new WaitForSeconds(_actionPause);
            if (unit.MoveLogic.Success)
            {
                unit.Move(unit.MoveLogic.Move);
            }
        }
        IsDone = true;
    }

    private IEnumerator AttackRoutine(ScriptableUnit unit)
    {
        IsDone = false;
        if (unit.CanAttack)
        {
            var attack = unit.PossibleAttack();
            unit.AttackLogic.CalculateMove(unit, unit.Grid);
            yield return new WaitForSeconds(_actionPause);
            if (unit.AttackLogic.Success)
            {
                unit.Attack(unit.AttackLogic.Move);
            }
        }
        IsDone = true;
    }

    public void Clear()
    {
        StopAllCoroutines();
    }
}
