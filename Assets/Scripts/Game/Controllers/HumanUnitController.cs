﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// human controlled units
/// </summary>
public class HumanUnitController : MonoBehaviour, IUnitContoller
{

    private SelectionControl _selection;
    // Use this for initialization
    private void Awake()
    {
        _selection = FindObjectOfType<SelectionControl>();
    }

    public bool IsDone { get; private set; }

    public void Attack(ScriptableUnit unit)
    {
        StartCoroutine(AttackRoutine(unit));
    }

    public void MoveUnit(ScriptableUnit unit)
    {
        StartCoroutine(MoveRoutine(unit));
    }

    private IEnumerator MoveRoutine(ScriptableUnit unit)
    {
        IsDone = false;
        if (unit.CanMove)
        {
            _selection.HighlightPosition(unit.Position);
            _selection.SelectTile(unit.PossibleMove());
            yield return new WaitUntil(() => _selection.IsDone);

            if (!_selection.IsCanceled)
            {
                unit.Move(_selection.SelectedTile);
            }
        }
        IsDone = true;
    }

    private IEnumerator AttackRoutine(ScriptableUnit unit)
    {
        IsDone = false;
        if (unit.CanAttack)
        {
            _selection.HighlightPosition(unit.Position);
            _selection.SelectTile(unit.PossibleAttack());
            yield return new WaitUntil(() => _selection.IsDone);
            if (!_selection.IsCanceled)
            {
                unit.Attack(_selection.SelectedTile);
            }
        }
        IsDone = true;
    }

    public void Clear()
    {
        _selection.Clear();
    }
}
