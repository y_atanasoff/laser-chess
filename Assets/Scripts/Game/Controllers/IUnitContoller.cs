﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// interface for controlling the units in the game
/// </summary>
public interface IUnitContoller
{
    /// <summary>
    /// is the move done, can we make the next move?
    /// </summary>
    bool IsDone { get; }
    /// <summary>
    /// order a move
    /// </summary>
    void MoveUnit(ScriptableUnit unit);
    /// <summary>
    /// order an attack
    /// </summary>
    void Attack(ScriptableUnit unit);

    void Clear();
}
