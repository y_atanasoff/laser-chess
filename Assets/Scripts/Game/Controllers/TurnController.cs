﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// is responsigle for how the turn for each player is played out
/// </summary>
public class TurnController : MonoBehaviour
{
    [SerializeField]
    private Player _player;
    [SerializeField]
    private SelectionType _selectionType;

    public Player player { get { return _player; } }

    public ScriptableUnit CurrentUnit { get; private set; }

    private List<ScriptableUnit> _units = new List<ScriptableUnit>();
    /// <summary>
    /// is the player making a move
    /// </summary>
    public bool IsMakingMove { get; private set; }
    /// <summary>
    /// is the turn over - have all the units moved?
    /// </summary>
    public bool IsOver { get { return !_unitSelector.HasMoreMoves; } }
    /// <summary>
    /// used for pausing the logic while the view moves
    /// </summary>
    public bool isPaused { get; set; }

    private IUnitContoller _unitController;
    private ISelector _unitSelector;

    private void Awake()
    {
        _unitController = GetComponentInChildren(typeof(IUnitContoller)) as IUnitContoller;
        _unitSelector = SelectorFactotry.CreateSelector(_selectionType);
    }

    public void MakeTurn()
    {
        StartCoroutine(TurnRoutine());
    }

    public void StartTurn(ScriptableUnit[] units)
    {
        _unitSelector.ResetSelector(units);
    }

    private IEnumerator TurnRoutine()
    {
        IsMakingMove = true;
        CurrentUnit = _unitSelector.SelectNextUnit();

        _unitController.MoveUnit(CurrentUnit);
        yield return new WaitUntil(() => _unitController.IsDone && !isPaused);

        _unitController.Attack(CurrentUnit);
        yield return new WaitUntil(() => _unitController.IsDone && !isPaused);
        IsMakingMove = false;
    }

    public void Clear()
    {
        StopAllCoroutines();
        _unitSelector.ResetSelector(null);
        _unitController.Clear();
        CurrentUnit = null;
    }

}
