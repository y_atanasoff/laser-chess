﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBuilder : MonoBehaviour, IGridFactory
{

    [SerializeField]
    private GameObject _gridPrefab;
    public void BuildGrid(ScriptableGrid grid, Map map)
    {
        for(int x = 0;x < grid.Terrain.Size.x; x++)
        {
            for (int y = 0; y < grid.Terrain.Size.y; y++)
            {
                Instantiate(_gridPrefab, 
                            map.MapTransform.LevelToMap(new Vector2Int(x, y)), 
                            Quaternion.identity,
                            map.MapTransform.MapParent);
            }
        }
    }
}
