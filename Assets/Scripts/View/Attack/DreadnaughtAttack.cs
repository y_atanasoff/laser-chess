﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DreadnaughtAttack : MonoBehaviour, IViewAttack
{
    [Serializable]
    public class GunTarget
    {
        public GameObject Gun;
        public GameObject Target;
    }
    [SerializeField]
    private GameObject _projectile;
    [SerializeField]
    private GunTarget[] _targets;
    [SerializeField]
    private float _attackTime;

    public void Attack(Vector3 position, Vector3[] possiblePositions, Unit[] attackedUnits, UnityAction onComlete)
    {
        StartCoroutine(AttackRoutine(attackedUnits, onComlete));
    }

    public IEnumerator AttackRoutine(Unit[] attackedUnits, UnityAction onComlete)
    {
        for (int i = 0; i < _targets.Length; i++)
        {
            var proj = Instantiate(_projectile, transform).GetComponent<Projectile>();
            if (proj != null)
            {
                proj.LaunchProjectile(_targets[i].Gun.transform.position,
                                      _targets[i].Target.transform.position, _attackTime);
            }
        }
        yield return new WaitForSeconds(_attackTime);
        foreach (var unit in attackedUnits)
        {
            unit.TakeDamage();
        }
        onComlete.Invoke();
    }

}
