﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IViewAttack
{
    void Attack(Vector3 position, Vector3[] possiblePositions, Unit[] attackedUnits, UnityAction onComlete);
}
