﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class JumpShipAttack : MonoBehaviour, IViewAttack
{
    [SerializeField]
    private GameObject[] _knuckles;
    [SerializeField]
    private float _hitTime;
    [SerializeField]
    private float _returnTime;

    public void Attack(Vector3 position, Vector3[] possiblePositions, Unit[] attackedUnits, UnityAction onComplete)
    {
        StartCoroutine(AttackRoutine(position, attackedUnits, onComplete));
    }

    public IEnumerator AttackRoutine(Vector3 position, Unit[] attackedUnits, UnityAction onComplete)
    {
        yield return StartCoroutine(OffsetWithPosWithSpeed(1.0f, _hitTime));
        foreach (var unit in attackedUnits)
        {
            unit.TakeDamage();
        }
        yield return StartCoroutine(OffsetWithPosWithSpeed(-1.0f, _returnTime));
        onComplete.Invoke();
    }

    private IEnumerator OffsetWithPosWithSpeed(float amount, float speed)
    {
        Coroutine coroutine = null;
        for (int i = 0; i < _knuckles.Length; i++)
        {

            var knuckle = _knuckles[i];

            coroutine = StartCoroutine(Coroutines.MoveTowards(knuckle.transform,
                    knuckle.transform.position + amount * knuckle.transform.forward, speed));
        }
        yield return coroutine;
    }

    private Unit FindUnitInDirection(Vector3 origin, Vector3 dir, float maxDistance)
    {
        RaycastHit info;
        if (Physics.Raycast(origin, dir, out info, maxDistance, LayerMask.GetMask("Unit")))
        {
            var unit = info.collider.GetComponent<Unit>();
            if(unit != null)
            {
                return unit;
            }

        }
        return null;
    }

}
