﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BasicAttack : MonoBehaviour, IViewAttack
{
    [SerializeField]
    private float _rotateTime;
    [SerializeField]
    private GameObject _projectile;
    [SerializeField]
    private GameObject[] _guns;
    [SerializeField]
    private float _attackTime;

    public void Attack(Vector3 position, Vector3[] possiblePositions, Unit[] attackedUnits, UnityAction onComlete)
    {
        StartCoroutine(AttackRoutine(position, attackedUnits, onComlete));
    }

    public IEnumerator AttackRoutine(Vector3 position, Unit[] attackedUnits, UnityAction onComlete)
    {
        if (attackedUnits != null && attackedUnits.Length > 0)
        {
            Vector3 forward = transform.forward;
            var vector = (position - transform.position).normalized;

            yield return StartCoroutine(Coroutines.RotateTowards(transform, position, _rotateTime));
            foreach (var gun in _guns)
            {
                var proj = Instantiate(_projectile, transform).GetComponent<Projectile>();
                if (proj != null)
                {
                    proj.LaunchProjectile(gun.transform.position,
                                            attackedUnits[0].transform.position + Vector3.up * 0.5f,
                                            _attackTime);
                }
            }

            yield return new WaitForSeconds(_attackTime);
            foreach (var unit in attackedUnits)
            {
                unit.TakeDamage();
            }

            yield return StartCoroutine(Coroutines.RotateTowards(transform, transform.position + forward,
                                                      _rotateTime * 2));
        }
        onComlete.Invoke();
    }

}
