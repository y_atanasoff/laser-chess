﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicDamage : MonoBehaviour, IViewDamage
{
    [SerializeField]
    private float _moveTime;

    public void TakeDamage(bool isDead)
    {
        StartCoroutine(TakeDamageCoroutine(isDead));
    }

    private IEnumerator TakeDamageCoroutine(bool isDead)
    {

        yield return StartCoroutine(
                     Coroutines.MoveTowards(transform,
                     transform.position - transform.forward * 0.3f,
                     _moveTime * 0.5f));

        yield return StartCoroutine(
                     Coroutines.MoveTowards(transform,
                     transform.position + transform.forward * 0.3f,
                     _moveTime * 0.5f));

        if (isDead)
        {
            Kill();
        }
    }
    private void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            Kill();
        }
    }

    protected virtual void Kill()
    {
        Destroy(gameObject);
    }


}
