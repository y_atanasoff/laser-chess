﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IViewDamage
{
    void TakeDamage(bool isDead);
}
