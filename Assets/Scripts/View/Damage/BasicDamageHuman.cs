﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicDamageHuman : BasicDamage
{
    [SerializeField]
    private float _disasembleTime;
    [SerializeField]
    private float KillPause;

    protected override void Kill()
    {
        StartCoroutine(KillRoutine());
    }

    private IEnumerator KillRoutine()
    {
        var renderers = GetComponentsInChildren<MeshRenderer>();
        float pause = _disasembleTime / renderers.Length;
        foreach (var rend in renderers)
        {
            StartCoroutine(FadeToBlack(rend));
            yield return new WaitForSeconds(pause);
        }
        yield return new WaitForSeconds(KillPause);
        Destroy(gameObject);
    }

    private IEnumerator FadeToBlack(Renderer rend)
    {
        while (rend.material.color.r > 0.01f)
        {
            rend.material.color = Color.Lerp(rend.material.color, Color.black, 0.1f);
            yield return null;
        }
    }


}
