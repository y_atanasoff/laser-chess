﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicDamageAI : BasicDamage
{
    [SerializeField]
    private float _disasembleTime;
    [SerializeField]
    private float _pause;
    protected override void Kill()
    {
        StartCoroutine(KillRoutine());
    }

    private IEnumerator KillRoutine()
    {
        var renderers = GetComponentsInChildren<MeshRenderer>();
        float pause = _disasembleTime / renderers.Length;
        foreach (var rend in renderers)
        {
            var body = rend.gameObject.AddComponent<Rigidbody>();
            yield return new WaitForSeconds(pause);
        }
        yield return new WaitForSeconds(_pause);
        Destroy(gameObject);
    }


}
