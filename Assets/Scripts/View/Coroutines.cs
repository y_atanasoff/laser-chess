﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Coroutines
{
    public static IEnumerator RotateTowards(Transform tr, Vector3 position, float time)
    {
        var lookTowards = Quaternion.LookRotation(
                        (position - tr.position).normalized,
                        Vector3.up);

        float speed = Quaternion.Angle(tr.rotation, lookTowards) / time;
        while (time > 0)
        {
            tr.rotation = Quaternion.RotateTowards(tr.rotation, lookTowards, speed * Time.deltaTime);
            time -= Time.deltaTime;
            yield return null;
        }
        tr.localRotation = lookTowards;
    }

    public static IEnumerator MoveTowards(Transform tr, Vector3 position, float time)
    {
        float speed = Vector3.Distance(tr.position, position) / time;
        while (time > 0)
        {
            tr.position = Vector3.MoveTowards(tr.position, position, speed * Time.deltaTime);
            time -= Time.deltaTime;
            yield return null;
        }
        tr.position = position;
    }
}
