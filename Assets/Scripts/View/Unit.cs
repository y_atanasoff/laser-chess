﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Unit : MonoBehaviour, IUnitView
{

    private HealthBar _healthBar;

    private Map _map;
    private Transform _transform;
    private ScriptableUnit _model;

    private IViewMovement _movement;
    private IViewAttack _attack;
    private IViewDamage _damage;

    private void Start()
    {
        _transform = GetComponent<Transform>();
        _movement = (IViewMovement)GetComponentInChildren(typeof(IViewMovement));
        _attack = (IViewAttack)GetComponentInChildren(typeof(IViewAttack));
        _damage = (IViewDamage)GetComponentInChildren(typeof(IViewDamage));
        _healthBar = GetComponentInChildren<HealthBar>();
        _healthBar.MaxHealth = _model.Health;
    }

    public void Init(Map map, ScriptableUnit model)
    {
        _map = map;
        _model = model;
        _model.OnUnitMoved.AddListener(Move);
        _model.OnUnitAttack.AddListener(Attack);
    }

    public void Move(Vector2Int position)
    {
        _map.isPaused = true;
        _movement.Move(_map.MapTransform.LevelToMap(position), OnActionComplete);
    }

    public void Attack(Vector2Int[] possbieHits, Vector2Int attacked, ScriptableUnit[] attackedUnits)
    {
        if (_attack != null)
        {
            _map.isPaused = true;
            var hits = new List<Vector3>();
            foreach (var hit in possbieHits)
            {
                hits.Add(_map.MapTransform.LevelToMap(hit));
            }

            var units = new List<Unit>();
            foreach (var attackedUnit in attackedUnits)
            {
                units.Add(_map.GetView(attackedUnit).GetComponent<Unit>());
            }

            _attack.Attack(_map.MapTransform.LevelToMap(attacked), hits.ToArray(), units.ToArray(), OnActionComplete);
        }
    }

    public void TakeDamage()
    {
        _healthBar.CurrentHealth = _model.Health;
        _damage.TakeDamage(_model.Health <= 0);
    }

    private void OnActionComplete()
    {
        _map.isPaused = false;
    }
}
