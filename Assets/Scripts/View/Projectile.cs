﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField]
    private LineRenderer _renderer;
    [SerializeField]
    private Rigidbody _body;
    private Vector3[] _positions = { Vector3.zero, Vector3.zero };

    private Vector3 _destination = Vector3.zero;
    private Vector3 _start;
    private float _travelTime = 0;
    private bool _initialized = false;
    public Unit Target { get; private set; }
    private void Start()
    {
        _renderer.SetPositions(_positions);
        if (_initialized)
        {
            StartCoroutine(Launch());
        }
        _initialized = true;
    }


    public void LaunchProjectile(Vector3 from, Vector3 to, float travelTime)
    {
        _start = from;
        _destination = to;
        
        _travelTime = travelTime * 2;
        if (_initialized)
        {
            _renderer.SetPositions(_positions);
            StartCoroutine(Launch());
        }
        _initialized = true;
    }

    private IEnumerator Launch()
    {
        _positions[0] = _positions[1] = _start;
        float speed = Vector3.Distance(_start, _destination) / (_travelTime * 0.5f);
        float time = _travelTime * 0.5f;

        while (time > 0)
        {
            _positions[1] = Vector3.MoveTowards(_positions[1], _destination, speed * Time.deltaTime);
            time -= Time.deltaTime;
            _renderer.SetPositions(_positions);
            yield return null;
        }

        time = _travelTime * 0.5f;
        while (time > 0)
        {
            _positions[0] = Vector3.MoveTowards(_positions[0], _destination, speed * Time.deltaTime);
            time -= Time.deltaTime;
            _renderer.SetPositions(_positions);
            yield return null;
        }
        Destroy(gameObject);
    }
}
