﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BasicMovement : MonoBehaviour, IViewMovement
{
    [SerializeField]
    private float _rotateTime;
    [SerializeField]
    private float _moveTime;
    [SerializeField]
    private bool _rotate;

    public void Move(Vector3 position, UnityAction onComplete)
    {
        StartCoroutine(MoveRoutine(position, onComplete));
    }

    public IEnumerator MoveRoutine(Vector3 position, UnityAction onComplete)
    {
        Vector3 forward = transform.forward;
        if (_rotate)
        {
            yield return StartCoroutine(Coroutines.RotateTowards(transform, position, _rotateTime * 0.5f));
        }
        yield return StartCoroutine(Coroutines.MoveTowards(transform, position, _moveTime));
        if (_rotate)
        {
            yield return StartCoroutine(Coroutines.RotateTowards(transform, transform.position + forward, _rotateTime *0.5f));
        }
        onComplete.Invoke();
    }
}
