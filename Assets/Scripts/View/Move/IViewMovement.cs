﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IViewMovement
{
    void Move(Vector3 position, UnityAction onComplete);
}
