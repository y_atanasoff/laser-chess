﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TeleportMovement : MonoBehaviour, IViewMovement
{

    [SerializeField]
    private float _pause;
    private Renderer[] _renderers;

    private void Start()
    {
        _renderers = GetComponentsInChildren<Renderer>();
    }

    public void Move(Vector3 position, UnityAction onComplete)
    {
        StartCoroutine(MoveRoutine(position, onComplete));
    }

    public IEnumerator MoveRoutine(Vector3 position, UnityAction onComplete)
    {
        foreach (var renderer in _renderers)
        {
            renderer.enabled = false;
        }
        yield return new WaitForSeconds(_pause);
        transform.position = position;
        foreach (var renderer in _renderers)
        {
            renderer.enabled = true;
        }
        onComplete.Invoke();
    }
}
