﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wobble : MonoBehaviour
{
    [SerializeField]
    protected float _speed;
    [SerializeField]
    protected float _amount;

    protected float _offset;
    private Vector3 _originalPosition;
    private void Awake()
    {
        _offset = Random.value;
        _originalPosition = transform.localPosition;
    }

    public float GetOffset(float time)
    {
        return ( _amount * Mathf.Sin(time * _speed + _offset));
    }

    private void Update()
    {
        transform.localPosition = _originalPosition + Vector3.up * GetOffset(Time.time);
    }
}
